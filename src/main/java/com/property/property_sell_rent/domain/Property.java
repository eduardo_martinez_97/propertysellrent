package com.property.property_sell_rent.domain;

import com.property.property_sell_rent.model.PropertyState;
import com.property.property_sell_rent.model.PropertyType;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class Property {

    @Id
    @Column(nullable = false, updatable = false)
    @SequenceGenerator(
            name = "primary_sequence",
            sequenceName = "primary_sequence",
            allocationSize = 1,
            initialValue = 10000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "primary_sequence"
    )
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PropertyType type;

    @Column(nullable = false)
    private Double area;

    @Column(precision = 10, scale = 2)
    private BigDecimal price;

    @Column(unique = true, precision = 10, scale = 2)
    private BigDecimal energyPrice;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PropertyState state;

    @Column
    private Boolean isEquiped;

    @Column(length = 455)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_property_id", nullable = false)
    private User userProperty;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_category_id", nullable = false)
    private PropertyCategory propertyCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_address_id", nullable = false)
    private Address propertyAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_details_id")
    private PropertyDetails propertyDetails;

}
