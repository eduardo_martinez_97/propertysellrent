package com.property.property_sell_rent.domain;

import com.property.property_sell_rent.model.PropertyMainCategory;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class PropertyCategory {

    @Id
    @Column(nullable = false, updatable = false)
    @SequenceGenerator(
            name = "primary_sequence",
            sequenceName = "primary_sequence",
            allocationSize = 1,
            initialValue = 10000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "primary_sequence"
    )
    private Long id;

    @Column(nullable = false)
    private String subCategoryName;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PropertyMainCategory mainCategory;

    @OneToMany(mappedBy = "propertyCategory")
    private Set<Property> propertyCategoryPropertys;

}
