package com.property.property_sell_rent.domain;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.Getter;
import lombok.Setter;


@Entity
@Getter
@Setter
public class PropertyDetails {

    @Id
    @Column(nullable = false, updatable = false)
    @SequenceGenerator(
            name = "primary_sequence",
            sequenceName = "primary_sequence",
            allocationSize = 1,
            initialValue = 10000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "primary_sequence"
    )
    private Long id;

    @Column
    private Boolean hasElevator;

    @Column
    private Integer roomCount;

    @Column
    private Integer floor;

    @Column
    private Integer floorCount;

    @Column
    private Integer balconyCount;

    @Column
    private String energyCertificate;

    @OneToMany(mappedBy = "propertyDetails")
    private Set<Property> propertyDetailsPropertys;

}
