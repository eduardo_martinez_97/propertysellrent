package com.property.property_sell_rent.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EntityScan("com.property.property_sell_rent.domain")
@EnableJpaRepositories("com.property.property_sell_rent.repos")
@EnableTransactionManagement
public class DomainConfig {
}
