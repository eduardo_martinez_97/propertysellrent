package com.property.property_sell_rent.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AddressDTO {

    private Long id;

    @NotNull
    @Size(max = 255)
    private String street;

    @NotNull
    private Integer number;

    @NotNull
    @Size(max = 255)
    private String city;

    @NotNull
    @Size(max = 255)
    private String postalCode;

    @Size(max = 255)
    private String region;

    @NotNull
    @Size(max = 255)
    private String country;

}
