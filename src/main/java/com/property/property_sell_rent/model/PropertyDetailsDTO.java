package com.property.property_sell_rent.model;

import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PropertyDetailsDTO {

    private Long id;

    private Boolean hasElevator;

    private Integer roomCount;

    private Integer floor;

    private Integer floorCount;

    private Integer balconyCount;

    @Size(max = 255)
    private String energyCertificate;

}
