package com.property.property_sell_rent.model;


public enum PropertyMainCategory {

    PROPERTY_HOUSE,
    PROPERTY_FLAT,
    PROPERTY_LAND,
    PROPERTY_COMMERCIAL

}
