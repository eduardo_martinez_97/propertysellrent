package com.property.property_sell_rent.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PropertyCategoryDTO {

    private Long id;

    @NotNull
    @Size(max = 255)
    private String subCategoryName;

    @NotNull
    private PropertyMainCategory mainCategory;

}
