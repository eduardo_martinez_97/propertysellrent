package com.property.property_sell_rent.model;


public enum PropertyType {

    PROPERTY_SELL,
    PROPERTY_RENT,
    PROPERTY_BUY

}
