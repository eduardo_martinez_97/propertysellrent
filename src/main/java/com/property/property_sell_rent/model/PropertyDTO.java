package com.property.property_sell_rent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PropertyDTO {

    private Long id;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    private PropertyType type;

    @NotNull
    private Double area;

    @Digits(integer = 10, fraction = 2)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Schema(type = "string", example = "71.08")
    private BigDecimal price;

    @Digits(integer = 10, fraction = 2)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Schema(type = "string", example = "23.08")
    private BigDecimal energyPrice;

    @NotNull
    private PropertyState state;

    private Boolean isEquiped;

    @Size(max = 455)
    private String description;

    @NotNull
    private Long userProperty;

    @NotNull
    private Long propertyCategory;

    @NotNull
    private Long propertyAddress;

    private Long propertyDetails;

}
