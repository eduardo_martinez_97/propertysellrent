package com.property.property_sell_rent.rest;

import com.property.property_sell_rent.model.PropertyDTO;
import com.property.property_sell_rent.service.PropertyService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/propertys", produces = MediaType.APPLICATION_JSON_VALUE)
public class PropertyController {

    private final PropertyService propertyService;

    public PropertyController(final PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @GetMapping
    public ResponseEntity<List<PropertyDTO>> getAllPropertys() {
        return ResponseEntity.ok(propertyService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PropertyDTO> getProperty(@PathVariable final Long id) {
        return ResponseEntity.ok(propertyService.get(id));
    }

    @PostMapping
    public ResponseEntity<Long> createProperty(@RequestBody @Valid final PropertyDTO propertyDTO) {
        return new ResponseEntity<>(propertyService.create(propertyDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateProperty(@PathVariable final Long id,
            @RequestBody @Valid final PropertyDTO propertyDTO) {
        propertyService.update(id, propertyDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProperty(@PathVariable final Long id) {
        propertyService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
