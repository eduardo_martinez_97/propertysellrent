package com.property.property_sell_rent.rest;

import com.property.property_sell_rent.model.PropertyDetailsDTO;
import com.property.property_sell_rent.service.PropertyDetailsService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/propertyDetailss", produces = MediaType.APPLICATION_JSON_VALUE)
public class PropertyDetailsController {

    private final PropertyDetailsService propertyDetailsService;

    public PropertyDetailsController(final PropertyDetailsService propertyDetailsService) {
        this.propertyDetailsService = propertyDetailsService;
    }

    @GetMapping
    public ResponseEntity<List<PropertyDetailsDTO>> getAllPropertyDetailss() {
        return ResponseEntity.ok(propertyDetailsService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PropertyDetailsDTO> getPropertyDetails(@PathVariable final Long id) {
        return ResponseEntity.ok(propertyDetailsService.get(id));
    }

    @PostMapping
    public ResponseEntity<Long> createPropertyDetails(
            @RequestBody @Valid final PropertyDetailsDTO propertyDetailsDTO) {
        return new ResponseEntity<>(propertyDetailsService.create(propertyDetailsDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updatePropertyDetails(@PathVariable final Long id,
            @RequestBody @Valid final PropertyDetailsDTO propertyDetailsDTO) {
        propertyDetailsService.update(id, propertyDetailsDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePropertyDetails(@PathVariable final Long id) {
        propertyDetailsService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
