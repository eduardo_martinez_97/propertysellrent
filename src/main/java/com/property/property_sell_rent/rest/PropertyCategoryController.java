package com.property.property_sell_rent.rest;

import com.property.property_sell_rent.model.PropertyCategoryDTO;
import com.property.property_sell_rent.service.PropertyCategoryService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/propertyCategorys", produces = MediaType.APPLICATION_JSON_VALUE)
public class PropertyCategoryController {

    private final PropertyCategoryService propertyCategoryService;

    public PropertyCategoryController(final PropertyCategoryService propertyCategoryService) {
        this.propertyCategoryService = propertyCategoryService;
    }

    @GetMapping
    public ResponseEntity<List<PropertyCategoryDTO>> getAllPropertyCategorys() {
        return ResponseEntity.ok(propertyCategoryService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PropertyCategoryDTO> getPropertyCategory(@PathVariable final Long id) {
        return ResponseEntity.ok(propertyCategoryService.get(id));
    }

    @PostMapping
    public ResponseEntity<Long> createPropertyCategory(
            @RequestBody @Valid final PropertyCategoryDTO propertyCategoryDTO) {
        return new ResponseEntity<>(propertyCategoryService.create(propertyCategoryDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updatePropertyCategory(@PathVariable final Long id,
            @RequestBody @Valid final PropertyCategoryDTO propertyCategoryDTO) {
        propertyCategoryService.update(id, propertyCategoryDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePropertyCategory(@PathVariable final Long id) {
        propertyCategoryService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
