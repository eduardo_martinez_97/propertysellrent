package com.property.property_sell_rent.repos;

import com.property.property_sell_rent.domain.Property;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PropertyRepository extends JpaRepository<Property, Long> {
}
