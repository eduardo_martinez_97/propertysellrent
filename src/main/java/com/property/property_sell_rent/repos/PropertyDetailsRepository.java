package com.property.property_sell_rent.repos;

import com.property.property_sell_rent.domain.PropertyDetails;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PropertyDetailsRepository extends JpaRepository<PropertyDetails, Long> {
}
