package com.property.property_sell_rent.repos;

import com.property.property_sell_rent.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Long> {
}
