package com.property.property_sell_rent.repos;

import com.property.property_sell_rent.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AddressRepository extends JpaRepository<Address, Long> {
}
