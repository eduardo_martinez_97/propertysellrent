package com.property.property_sell_rent.repos;

import com.property.property_sell_rent.domain.PropertyCategory;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PropertyCategoryRepository extends JpaRepository<PropertyCategory, Long> {
}
