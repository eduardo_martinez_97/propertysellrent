package com.property.property_sell_rent.service;

import com.property.property_sell_rent.domain.Address;
import com.property.property_sell_rent.domain.Property;
import com.property.property_sell_rent.domain.PropertyCategory;
import com.property.property_sell_rent.domain.PropertyDetails;
import com.property.property_sell_rent.domain.User;
import com.property.property_sell_rent.model.PropertyDTO;
import com.property.property_sell_rent.repos.AddressRepository;
import com.property.property_sell_rent.repos.PropertyCategoryRepository;
import com.property.property_sell_rent.repos.PropertyDetailsRepository;
import com.property.property_sell_rent.repos.PropertyRepository;
import com.property.property_sell_rent.repos.UserRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
public class PropertyService {

    private final PropertyRepository propertyRepository;
    private final UserRepository userRepository;
    private final PropertyCategoryRepository propertyCategoryRepository;
    private final AddressRepository addressRepository;
    private final PropertyDetailsRepository propertyDetailsRepository;

    public PropertyService(final PropertyRepository propertyRepository,
            final UserRepository userRepository,
            final PropertyCategoryRepository propertyCategoryRepository,
            final AddressRepository addressRepository,
            final PropertyDetailsRepository propertyDetailsRepository) {
        this.propertyRepository = propertyRepository;
        this.userRepository = userRepository;
        this.propertyCategoryRepository = propertyCategoryRepository;
        this.addressRepository = addressRepository;
        this.propertyDetailsRepository = propertyDetailsRepository;
    }

    public List<PropertyDTO> findAll() {
        return propertyRepository.findAll()
                .stream()
                .map(property -> mapToDTO(property, new PropertyDTO()))
                .collect(Collectors.toList());
    }

    public PropertyDTO get(final Long id) {
        return propertyRepository.findById(id)
                .map(property -> mapToDTO(property, new PropertyDTO()))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Long create(final PropertyDTO propertyDTO) {
        final Property property = new Property();
        mapToEntity(propertyDTO, property);
        return propertyRepository.save(property).getId();
    }

    public void update(final Long id, final PropertyDTO propertyDTO) {
        final Property property = propertyRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        mapToEntity(propertyDTO, property);
        propertyRepository.save(property);
    }

    public void delete(final Long id) {
        propertyRepository.deleteById(id);
    }

    private PropertyDTO mapToDTO(final Property property, final PropertyDTO propertyDTO) {
        propertyDTO.setId(property.getId());
        propertyDTO.setName(property.getName());
        propertyDTO.setType(property.getType());
        propertyDTO.setArea(property.getArea());
        propertyDTO.setPrice(property.getPrice());
        propertyDTO.setEnergyPrice(property.getEnergyPrice());
        propertyDTO.setState(property.getState());
        propertyDTO.setIsEquiped(property.getIsEquiped());
        propertyDTO.setDescription(property.getDescription());
        propertyDTO.setUserProperty(property.getUserProperty() == null ? null : property.getUserProperty().getId());
        propertyDTO.setPropertyCategory(property.getPropertyCategory() == null ? null : property.getPropertyCategory().getId());
        propertyDTO.setPropertyAddress(property.getPropertyAddress() == null ? null : property.getPropertyAddress().getId());
        propertyDTO.setPropertyDetails(property.getPropertyDetails() == null ? null : property.getPropertyDetails().getId());
        return propertyDTO;
    }

    private Property mapToEntity(final PropertyDTO propertyDTO, final Property property) {
        property.setName(propertyDTO.getName());
        property.setType(propertyDTO.getType());
        property.setArea(propertyDTO.getArea());
        property.setPrice(propertyDTO.getPrice());
        property.setEnergyPrice(propertyDTO.getEnergyPrice());
        property.setState(propertyDTO.getState());
        property.setIsEquiped(propertyDTO.getIsEquiped());
        property.setDescription(propertyDTO.getDescription());
        if (propertyDTO.getUserProperty() != null && (property.getUserProperty() == null || !property.getUserProperty().getId().equals(propertyDTO.getUserProperty()))) {
            final User userProperty = userRepository.findById(propertyDTO.getUserProperty())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "userProperty not found"));
            property.setUserProperty(userProperty);
        }
        if (propertyDTO.getPropertyCategory() != null && (property.getPropertyCategory() == null || !property.getPropertyCategory().getId().equals(propertyDTO.getPropertyCategory()))) {
            final PropertyCategory propertyCategory = propertyCategoryRepository.findById(propertyDTO.getPropertyCategory())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "propertyCategory not found"));
            property.setPropertyCategory(propertyCategory);
        }
        if (propertyDTO.getPropertyAddress() != null && (property.getPropertyAddress() == null || !property.getPropertyAddress().getId().equals(propertyDTO.getPropertyAddress()))) {
            final Address propertyAddress = addressRepository.findById(propertyDTO.getPropertyAddress())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "propertyAddress not found"));
            property.setPropertyAddress(propertyAddress);
        }
        if (propertyDTO.getPropertyDetails() != null && (property.getPropertyDetails() == null || !property.getPropertyDetails().getId().equals(propertyDTO.getPropertyDetails()))) {
            final PropertyDetails propertyDetails = propertyDetailsRepository.findById(propertyDTO.getPropertyDetails())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "propertyDetails not found"));
            property.setPropertyDetails(propertyDetails);
        }
        return property;
    }

}
