package com.property.property_sell_rent.service;

import com.property.property_sell_rent.domain.PropertyDetails;
import com.property.property_sell_rent.model.PropertyDetailsDTO;
import com.property.property_sell_rent.repos.PropertyDetailsRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
public class PropertyDetailsService {

    private final PropertyDetailsRepository propertyDetailsRepository;

    public PropertyDetailsService(final PropertyDetailsRepository propertyDetailsRepository) {
        this.propertyDetailsRepository = propertyDetailsRepository;
    }

    public List<PropertyDetailsDTO> findAll() {
        return propertyDetailsRepository.findAll()
                .stream()
                .map(propertyDetails -> mapToDTO(propertyDetails, new PropertyDetailsDTO()))
                .collect(Collectors.toList());
    }

    public PropertyDetailsDTO get(final Long id) {
        return propertyDetailsRepository.findById(id)
                .map(propertyDetails -> mapToDTO(propertyDetails, new PropertyDetailsDTO()))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Long create(final PropertyDetailsDTO propertyDetailsDTO) {
        final PropertyDetails propertyDetails = new PropertyDetails();
        mapToEntity(propertyDetailsDTO, propertyDetails);
        return propertyDetailsRepository.save(propertyDetails).getId();
    }

    public void update(final Long id, final PropertyDetailsDTO propertyDetailsDTO) {
        final PropertyDetails propertyDetails = propertyDetailsRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        mapToEntity(propertyDetailsDTO, propertyDetails);
        propertyDetailsRepository.save(propertyDetails);
    }

    public void delete(final Long id) {
        propertyDetailsRepository.deleteById(id);
    }

    private PropertyDetailsDTO mapToDTO(final PropertyDetails propertyDetails,
            final PropertyDetailsDTO propertyDetailsDTO) {
        propertyDetailsDTO.setId(propertyDetails.getId());
        propertyDetailsDTO.setHasElevator(propertyDetails.getHasElevator());
        propertyDetailsDTO.setRoomCount(propertyDetails.getRoomCount());
        propertyDetailsDTO.setFloor(propertyDetails.getFloor());
        propertyDetailsDTO.setFloorCount(propertyDetails.getFloorCount());
        propertyDetailsDTO.setBalconyCount(propertyDetails.getBalconyCount());
        propertyDetailsDTO.setEnergyCertificate(propertyDetails.getEnergyCertificate());
        return propertyDetailsDTO;
    }

    private PropertyDetails mapToEntity(final PropertyDetailsDTO propertyDetailsDTO,
            final PropertyDetails propertyDetails) {
        propertyDetails.setHasElevator(propertyDetailsDTO.getHasElevator());
        propertyDetails.setRoomCount(propertyDetailsDTO.getRoomCount());
        propertyDetails.setFloor(propertyDetailsDTO.getFloor());
        propertyDetails.setFloorCount(propertyDetailsDTO.getFloorCount());
        propertyDetails.setBalconyCount(propertyDetailsDTO.getBalconyCount());
        propertyDetails.setEnergyCertificate(propertyDetailsDTO.getEnergyCertificate());
        return propertyDetails;
    }

}
