package com.property.property_sell_rent.service;

import com.property.property_sell_rent.domain.PropertyCategory;
import com.property.property_sell_rent.model.PropertyCategoryDTO;
import com.property.property_sell_rent.repos.PropertyCategoryRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
public class PropertyCategoryService {

    private final PropertyCategoryRepository propertyCategoryRepository;

    public PropertyCategoryService(final PropertyCategoryRepository propertyCategoryRepository) {
        this.propertyCategoryRepository = propertyCategoryRepository;
    }

    public List<PropertyCategoryDTO> findAll() {
        return propertyCategoryRepository.findAll()
                .stream()
                .map(propertyCategory -> mapToDTO(propertyCategory, new PropertyCategoryDTO()))
                .collect(Collectors.toList());
    }

    public PropertyCategoryDTO get(final Long id) {
        return propertyCategoryRepository.findById(id)
                .map(propertyCategory -> mapToDTO(propertyCategory, new PropertyCategoryDTO()))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Long create(final PropertyCategoryDTO propertyCategoryDTO) {
        final PropertyCategory propertyCategory = new PropertyCategory();
        mapToEntity(propertyCategoryDTO, propertyCategory);
        return propertyCategoryRepository.save(propertyCategory).getId();
    }

    public void update(final Long id, final PropertyCategoryDTO propertyCategoryDTO) {
        final PropertyCategory propertyCategory = propertyCategoryRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        mapToEntity(propertyCategoryDTO, propertyCategory);
        propertyCategoryRepository.save(propertyCategory);
    }

    public void delete(final Long id) {
        propertyCategoryRepository.deleteById(id);
    }

    private PropertyCategoryDTO mapToDTO(final PropertyCategory propertyCategory,
            final PropertyCategoryDTO propertyCategoryDTO) {
        propertyCategoryDTO.setId(propertyCategory.getId());
        propertyCategoryDTO.setSubCategoryName(propertyCategory.getSubCategoryName());
        propertyCategoryDTO.setMainCategory(propertyCategory.getMainCategory());
        return propertyCategoryDTO;
    }

    private PropertyCategory mapToEntity(final PropertyCategoryDTO propertyCategoryDTO,
            final PropertyCategory propertyCategory) {
        propertyCategory.setSubCategoryName(propertyCategoryDTO.getSubCategoryName());
        propertyCategory.setMainCategory(propertyCategoryDTO.getMainCategory());
        return propertyCategory;
    }

}
