package com.property.property_sell_rent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class PropertySellRentApplication {

    public static void main(String[] args) {
        SpringApplication.run(PropertySellRentApplication.class, args);
    }

}
